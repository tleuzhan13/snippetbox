module tleu.snippetbox.com

go 1.15

require (
	github.com/bmizerany/pat v0.0.0-20170815010413-6226ea591a40
	github.com/jackc/pgx/v4 v4.9.0
	github.com/justinas/alice v1.2.0
)
